
Job Board is a basic website used for allowing Employers to post jobs and users to apply.

USER INFO:

Normal Users:
user1@jobboard.co.uk
user2@jobboard.co.uk
user3@jobboard.co.uk

Employer Users:
employer1@jobboard.co.uk
employer2@jobboard.co.uk
employer3@jobboard.co.uk

Admin Users:
admin@jobboard.co.uk

Password: test

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.
