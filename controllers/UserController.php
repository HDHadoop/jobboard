<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\RegisterForm;
use app\models\User;
use app\models\Role;
use yii\helpers\ArrayHelper;

class UserController extends Controller
{
    /**
     * Used to display the register page. This allows a user to sign up to the website.
     * Uses the RegisterForm model for the fields as well as logic.
     */
    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            //TODO - When user dashboards are set up redirect them to the correct dashboard.
            return $this->goBack();
        }

        $roles = ArrayHelper::map(Role::find()->where(['!=', 'role', 'Admin'])->all(), 'id', 'role');

        return $this->render('register', [
            'model' => $model,
            'roles' => $roles
        ]);

        return $this->render('register');
    }

    /**
     * Used to display the contact page.
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Used to display the about page.
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
