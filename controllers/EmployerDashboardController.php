<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 09:15
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Location;
use yii\helpers\ArrayHelper;
use app\models\Job;
use app\models\JobForm;
use app\models\UserJobs;
use yii\data\Pagination;

class EmployerDashboardController extends Controller
{
    /**
     * Function used to do a check on start up.
     * This check is whether the user has the correct credentials.
     */
    public function init() {
        if(\Yii::$app->session->get('user.role_id') != 2) {
            return $this->goHome();
        }
    }

    /**
     * Used to display the index page.
     * Gets the jobs that the current user has posted as well as preparing the pagination.
     */
    public function actionIndex() {
        $jobs = Job::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['status' => 1]);
        $countQuery = clone $jobs;

        $pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $countQuery->count()]);

        $models = $jobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'jobs' => $models,
            'pages' => $pages
        ]);
    }

    /**
     * Used to display the Add page. Page is used to allow for the user to add jobs to the site.
     * Uses JobForm model in order to get all the required fields and logic.
     */
    public function actionAdd() {
        $model = new JobForm();
        $locations = ArrayHelper::map(Location::find()->all(), 'id', 'location');

        if ($model->load(Yii::$app->request->post()) && $model->add()) {
            //TODO - When user dashboards are set up redirect them to the correct dashboard.
            return $this->goBack();
        }

        return $this->render('add', [
            'model' => $model,
            'locations' => $locations
        ]);
    }

    /**
     * Used to display the edit page. Page is used to allow the user to edit their job listings.
     * Uses JobForm model in order to get all the required fields and logic.
     *
     * Param: int $id - Used to fetch the Job listing the user wishes to edit.
     */
    public function actionEdit($id) {
        $model = new JobForm();
        $job = Job::find()->where(['id' => $id])->one();
        $model->description = $job->description;
        $locations = ArrayHelper::map(Location::find()->all(), 'id', 'location');

        if ($model->load(Yii::$app->request->post()) && $model->edit($job)) {
            return $this->redirect(['employer-dashboard/index']);
        }

        return $this->render('edit', [
            'model' => $model,
            'job' => $job,
            'locations' => $locations
        ]);
    }

    /**
     * Used to display the applicants page. This allows the user to view the current applicants who have applied to their listing.
     * They are also given the option to download their CV as well as accept/reject their application.
     *
     * Param: int $id - Used to fetch the job listing's applicants.
     */
    public function actionApplicants($id) {
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if(isset($post['application-status'])) {
                $applicant = UserJobs::find()->where(['id' => $post['id']])->one();
                $applicant->status = $post['application-status'] == 1 ? 'Accepted' : 'Rejected';
                $applicant->save();
            } else {
                $file = UserJobs::find()->where(['id' => $post])->one();

                $path = 'uploads/' . $file->cv;

                if(file_exists($path)) {
                    return Yii::$app->response->sendFile($path);
                }
            }
        }
        $applied_users = UserJobs::find()->where(['job_id' => $id]);

        $countQuery = clone $applied_users;

        $pages = new Pagination(['defaultPageSize' => 10, 'totalCount' => $countQuery->count()]);

        $model = $applied_users->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('applicants', [
            'users' => $model,
            'pages' => $pages
        ]);
    }
}