<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 25/01/2018
 * Time: 19:22
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\UserJobs;
use yii\data\Pagination;

class UserDashboardController extends Controller
{
    /*
     * Function used to do a check on start up.
     * This check is whether the user has the correct credentials.
     */
    public function init() {
        if(\Yii::$app->session->get('user.role_id') != 1) {
            return $this->goHome();
        }
    }

    /*
     * Used to display the index page. This page allows the user to view all jobs they have applied for as well as their status.
     */
    public function actionIndex() {
        $applied_jobs = UserJobs::find()->where(['user_id' => Yii::$app->user->identity->id]);
        $countQuery = clone $applied_jobs;

        $pages = new Pagination(['defaultPageSize' => 10, 'totalCount' => $countQuery->count()]);

        $model = $applied_jobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index',[
            'jobs' => $model,
            'pages' => $pages
        ]);
    }
}