<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 15:10
 */

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Location;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Role;
use app\models\Job;
use yii\data\Pagination;

class AdminDashboardController extends Controller
{
    /**
     * Function used to do a check on start up.
     * This check is whether the user has the correct credentials.
     */
    public function init() {
        if(\Yii::$app->session->get('user.role_id') != 3) {
            return $this->goHome();
        }
    }

    /**
     * Used to display the index page of the admin controller.
     * This page shows basic stats of the website.
     * Number of normal and employer users + number of jobs listed on the site.
     */
    public function actionIndex() {
        $data['normal_users'] = User::find()->joinWith('role')->where(['user_roles.role_id' => 1])->count();
        $data['employer_users'] = User::find()->joinWith('role')->where(['user_roles.role_id' => 2])->count();
        $data['jobs'] = Job::find()->where(['status' => 1])->count();

        return $this->render('index', [
            'data' => $data
        ]);
    }

    /**
     * Used to display the users view.
     * This view shows a list of all the users on the site.
     * Also has a filter to filter between the types of users.
     */
    public function actionUsers() {
        $users = User::find()->where(['status' => 1]);

        $role = \Yii::$app->request->get('user-roles');

        if(!is_null($role)) {
            $users->joinWith('user_roles')->where(['user_roles.role_id' => $role]);
        }

        //TODO - Change for using sql.
        $countQuery = clone $users;

        if($users) {
            $roles = Role::find()->all();
        }

        $pages = new Pagination(['defaultPageSize' => 10, 'totalCount' => $countQuery->count()]);

        $model = $users->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('users', [
            'users' => $model,
            'roles' => $roles,
            'pages' => $pages
        ]);
    }

    /**
     * Used to show the jobs view.
     * This view shows all the current jobs posted on the site.
     * Also has a filter for filtering the jobs by tags.
     */
    public function actionJobs() {
        $jobs = Job::find()->where(['status' => 1])->orderBy('expiry_date');

        $tags = \Yii::$app->request->get('tag');

        if(!is_null($tags)) {
            $jobs->andWhere(['like','tags', $tags]);
        }

        $countQuery = clone $jobs;

        $pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $countQuery->count()]);

        $model = $jobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('jobs', [
            'jobs' => $model,
            'pages' => $pages
        ]);
    }
}