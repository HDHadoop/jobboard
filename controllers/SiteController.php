<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ApplyForm;
use app\models\User;
use app\models\Job;
use yii\web\UploadedFile;
use yii\data\Pagination;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays the home page to the website.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays the login page allowing for users to login.
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            //Based on the user's role id, redirect them to their appropriate dashboard.
            if(\Yii::$app->session->get('user.role_id') == 1) {
                return $this->redirect(['user-dashboard/index']);
            } else if(\Yii::$app->session->get('user.role_id') == 2) {
                return $this->redirect(['employer-dashboard/index']);
            } else if(\Yii::$app->session->get('user.role_id') == 3) {
                return $this->redirect(['admin-dashboard/index']);
            } else {
                return $this->goBack();
            }

        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Used to allow the user to logout.
     */
    public function actionLogout()
    {
        //User is logging out, destroy the session.
        \Yii::$app->session->remove('user.role_id');
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Used to allow users of the website to view the jobs posted.
     * Allows for filtering by tags.
     */
    public function actionJobs() {
        $jobs = Job::find()->where(['status' => 1])->orderBy('expiry_date');

        $tags = \Yii::$app->request->get('tag');

        if(!is_null($tags)) {
            $jobs->andWhere(['like','tags', $tags]);
        }

        $countQuery = clone $jobs;

        $pages = new Pagination(['defaultPageSize' => 5, 'totalCount' => $countQuery->count()]);

        $models = $jobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('jobs', [
            'jobs' => $models,
            'pages' => $pages
        ]);
    }

    /**
     * Used to display the view page allowing for users to see the full detailed view of the job listing selected.
     * param: int $id - Id of the job listing.
     */
    public function actionView($id) {
        $show_form = false;
        $submitted = false;
        if(!Yii::$app->user->isGuest && \Yii::$app->session->get('user.role_id') == 1) {
            $show_form = true;
        }

        $job = Job::find()->where(['id' => $id])->one();
        $model = new ApplyForm();
        if (Yii::$app->request->isPost) {
            $model->cv = UploadedFile::getInstance($model, 'cv');
            if ($model->apply($id)) {
                // file is uploaded successfully
                $submitted = true;
            }
        }

        return $this->render('view', [
            'job' => $job,
            'model' => $model,
            'show_form' => $show_form,
            'submitted' => $submitted
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
