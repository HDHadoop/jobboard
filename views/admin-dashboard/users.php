<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 15:48
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = 'Admin Dashboard - User List';
$this->params['breadcrumbs'][] = $this->title;

?>


<div id="admin-list" class="container center-block">
    <h1>User List</h1>

    <?php if($users): ?>
        <div class="filters">
            <form method="get" action="<?= htmlspecialchars(Yii::$app->request->url); ?>">
                <select name="user-roles">
                    <?php foreach($roles as $role): ?>
                        <option name="<?= Html::encode($role->role); ?>" value="<?= $role->id; ?>">
                            <?= Html::encode($role->role); ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <button type="submit">Filter</button>
            </form>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($users as $user): ?>
                    <tr>
                        <td><?= Html::encode($user->first_name) ?></td>
                        <td><?= Html::encode($user->last_name) ?></td>
                        <td><?= Html::encode($user->username) ?></td>
                        <td><?= Html::encode($user->phone) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <?=
            LinkPager::widget([
                'pagination' => $pages,
            ]);
        ?>

    <?php else: ?>
        <p>Currently there are no users.</p>
    <?php endif; ?>

</div>