<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 21:23
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Admin Dashboard - Job List';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">
    <h1>Job List</h1>

    <div class="filters">
        <form method="get" action="<?= htmlspecialchars(Yii::$app->request->url); ?>">
            <input type="text" name="tag" value="" />
            <button type="submit">Filter</button>
        </form>
    </div>

    <?php if($jobs): ?>
        <div class="job-container col-xs-12">
            <ul class="job-list">
                <?php foreach($jobs as $job): ?>

                    <li class="job-preview">
                        <div class="content float-left col-xs-12 col-sm-6">
                            <h4 class="job-title">
                                <?= Html::encode($job->title); ?>
                            </h4>
                            <h5 class="company">
                                <?= Html::encode($job->location->location) ?>
                            </h5>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="options-container float-right">
                                <div class="col-xs-12">
                                    <a href="<?= Url::toRoute(['site/view', 'id' => $job->id]); ?>" class="btn btn-apply no-margin float-right">
                                        View
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?=
               LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>

        </div>
    <?php else: ?>
        <p>Sorry, there are currently no jobs. Please check back later.</p>
    <?php endif; ?>
</div>
