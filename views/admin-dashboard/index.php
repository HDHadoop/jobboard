<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 15:10
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Admin Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>


<div id="admin-home" class="container center-block">
    <h1>Your Dashboard</h1>


    <div class="col-xs-12 col-md-4 text-center stat-container">
        <label>Normal Users</label>
        <span><?= $data['normal_users'] ?></span>
        <a href="<?= Url::toRoute(['admin-dashboard/users']); ?>/?user-roles=1">
            <button class="btn btn-primary">View</button>
        </a>
    </div>
    <div class="col-xs-12 col-md-4 text-center stat-container">
        <label>Employer Users</label>
        <span><?= $data['employer_users'] ?></span>
        <a href="<?= Url::toRoute(['admin-dashboard/users']); ?>/?user-roles=2">
            <button class="btn btn-primary">View</button>
        </a>
    </div>
    <div class="col-xs-12 col-md-4 text-center stat-container">
        <label>Jobs Posted</label>
        <span><?= $data['jobs'] ?></span>
        <a href="<?= Url::toRoute(['admin-dashboard/jobs']); ?>">
            <button class="btn btn-primary">View</button>
        </a>
    </div>
</div>