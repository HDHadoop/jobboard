<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 09:16
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

$this->title = 'Employer Dashboard - Add Job';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="container center-block">
    <h1>Add Job</h1>
</div>

<div class="container">
    <?php $form = ActiveForm::begin([
        'id' => 'job-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <?= $form->field($model, 'company_name')->textInput(['autofocus' => true])->textInput(['value' => $job->company_name]) ?>

    <?= $form->field($model, 'title')->textInput(['value' => $job->title]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>

    <?= $form->field($model, 'tags')->textInput(['value' => $job->tags]) ?>

    <?= $form->field($model, 'salary')->textInput(['value' => $job->salary]) ?>

    <?=
        $form->field($model, 'expiry_date')->widget(\yii\jui\DatePicker::className(), [
            'dateFormat' => 'dd/MM/yyyy',
        ])->textInput(['value' => date('d/m/Y', strtotime($job->expiry_date))])
    ?>

    <?=
    $form->field($model, 'location_id')
        ->dropDownList(
            $locations,           // Flat array ('id'=>'label')
            ['options' => [$job->location_id => ['selected'=>true]]]
        );
    ?>

    <div class="form-group">
        <div class="col-lg-offset-6 col-lg-6">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'update-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>