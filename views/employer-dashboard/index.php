<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 26/01/2018
 * Time: 09:16
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Employer Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="container center-block">
    <div class="col-xs-12 col-sm-6">
        <h1>Your Dashboard</h1>
    </div>
    <div class="col-xs-12 col-sm-6">
        <div class="float-right button-container">
            <a href="<?= Url::toRoute(['employer-dashboard/add']); ?>">
                <button class="btn btn-primary">
                    Add Job Posting
                </button>
            </a>
        </div>
    </div>
</div>

<div class="container">
    <?php if($jobs): ?>
        <div class="job-container col-xs-12">
            <ul class="job-list">
            <?php foreach($jobs as $job): ?>

                <li class="job-preview">
                    <div class="content float-left col-xs-12 col-sm-6">
                        <h4 class="job-title">
                            <?= Html::encode($job->title); ?>
                        </h4>
                        <h5 class="company">
                            <?= Html::encode($job->location->location) ?>
                        </h5>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="options-container float-right">
                            <div class="col-xs-12">
                                <a href="<?= Url::toRoute(['employer-dashboard/edit', 'id' => $job->id]); ?>" class="btn btn-apply float-right">
                                    Edit
                                </a>
                            </div>
                            <div class="col-xs-12">
                                <a href="<?= Url::toRoute(['employer-dashboard/applicants', 'id' => $job->id]); ?>" class="btn btn-apply no-margin float-right">
                                    View
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
            </ul>

            <?=
                LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>
        </div>
    <?php else: ?>
        <p>Currently you have no jobs posted.</p>
    <?php endif; ?>
</div>