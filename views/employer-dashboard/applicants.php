<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 29/01/2018
 * Time: 11:53
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Applicants';
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="employer-applicants" class="container center-block">
    <div class="col-xs-12">
        <h1>Applicants</h1>
    </div>

    <?php if($users): ?>
        <div class="job-container col-xs-12">
            <ul class="job-list">
                <?php foreach($users as $user): ?>

                    <li class="job-preview">
                        <div class="content float-left col-xs-12 col-sm-6">
                            <h4 class="job-title">
                                <?= Html::encode($user->user->first_name); ?>
                            </h4>
                            <h5 class="company">
                                <?= Html::encode($user->user->last_name) ?>
                            </h5>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="options-container float-right">
                                <div class="col-xs-12 text-right">
                                    <label>Status:</label>
                                    <span><?= Html::encode($user->status) ?></span>
                                </div>

                                <div class="col-xs-12">
                                    <form method="post" action="<?= htmlspecialchars(Yii::$app->request->url); ?>">
                                        <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                               value="<?=Yii::$app->request->csrfToken?>"/>
                                        <input name="id" value="<?= $user->id ?>" type="hidden" />
                                        <button class="btn btn-primary float-right">
                                            Download CV
                                        </button>
                                    </form>
                                </div>
                                <?php if($user->status == 'Pending'): ?>
                                    <div class="col-xs-12 text-right float-right">
                                        <form method="post" action="<?= htmlspecialchars(Yii::$app->request->url); ?>">
                                            <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                                   value="<?=Yii::$app->request->csrfToken?>"/>
                                            <input name="id" value="<?= $user->id ?>" type="hidden" />
                                            <input name="application-status" value="1" type="hidden" />
                                            <button class="btn btn-primary">
                                                Accept
                                            </button>
                                        </form>
                                        <form method="post" action="<?= htmlspecialchars(Yii::$app->request->url); ?>">
                                            <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                                                   value="<?=Yii::$app->request->csrfToken?>"/>
                                            <input name="id" value="<?= $user->id ?>" type="hidden" />
                                            <input name="application-status" value="0" type="hidden" />
                                            <button class="btn btn-primary">
                                                Reject
                                            </button>
                                        </form>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?=
                LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>

        </div>
    <?php else: ?>
        <p>
            Currently there are no applicants.
        </p>
    <?php endif; ?>
</div>
