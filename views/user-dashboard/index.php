<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 25/01/2018
 * Time: 19:24
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'User Dashboard';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="container center-block">
    <h1>Your Dashboard</h1>
    <h4>Jobs you've applied for:</h4>
    <?php if($jobs): ?>
        <div class="job-container col-xs-12">
            <ul class="job-list">
                <?php foreach($jobs as $job): ?>

                    <li class="job-preview">
                        <div class="content float-left col-xs-12 col-sm-6">
                            <h4 class="job-title">
                                <?= Html::encode($job->job->title); ?>
                            </h4>
                            <h5 class="company">
                                <?= Html::encode($job->job->location->location) ?>
                            </h5>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="options-container float-right">
                                <div class="col-xs-12 text-right">
                                    <label>Status:</label>
                                    <span><?= Html::encode($job->status) ?></span>
                                </div>

                                <div class="col-xs-12">
                                    <a href="<?= Url::toRoute(['site/view', 'id' => $job->job->id]); ?>" class="btn btn-apply no-margin float-right">
                                        View
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>

            <?=
                LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?>

        </div>
    <?php else: ?>
        <p>
            Currently you have not applied for any jobs.
            <a href="<?= Yii::$app->homeUrl ?>">
                Click here to find some!
            </a>
        </p>
    <?php endif; ?>
</div>