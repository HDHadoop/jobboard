<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'JobBoard Home';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to JobBoard.</h1>

        <p class="lead">We are here to help you find the perfect job.</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::toRoute(['site/jobs']); ?>">Get started Here.</a></p>
    </div>
</div>
