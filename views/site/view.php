<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 28/01/2018
 * Time: 12:29
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Job';
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="individual-job" class="container">
    <div class="text-center main-info">
        <h1><?= Html::encode($job->title);  ?></h1>
        <label>Company: <span class="no-bold"><?= Html::encode($job->company_name);  ?></span></label>
        <label>Location: <span class="no-bold"><?= Html::encode($job->location->location);  ?></span></label>
        <label>Salary: <span class="no-bold"><?= Html::encode($job->salary);  ?></span></label>
        <label>Date: <span class="no-bold"><?= Html::encode(date("d/m/Y", strtotime($job->expiry_date)));  ?></span></label>
    </div>
    <div class="description">
        <h2>Description:</h2>
        <p>
            <?= Html::encode($job->description);  ?>
        </p>
    </div>
    <div class="tags">
        <h2>Tags:</h2>
        <p>
            <?= Html::encode($job->tags);  ?>
        </p>
    </div>
</div>

<div class="apply-form container">
    <?php if($show_form): ?>
        <?php if($submitted): ?>
            <p>Thank you for applying, your cv has been sent.</p>
        <?php else: ?>
            <h2>Apply</h2>

            <p>Please upload your CV to apply:</p>

            <?php $form = ActiveForm::begin([
                'id' => 'apply-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'cv')->fileInput() ?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Apply', ['class' => 'btn btn-primary', 'name' => 'apply-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    <?php else: ?>
        <p>Please be logged in to apply.</p>
    <?php endif; ?>
</div>