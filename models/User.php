<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 25/01/2018
 * Time: 11:41
 */

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    //Finds a user by username.
    public static function checkUserExistsByUsername($email)
    {
        $user_count = User::find()->where(['username' => $email])->count();

        return $user_count;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public static function findByEmail($email)
    {
        $user = User::find()->where(['username' => $email])->one();

        return $user;
    }

    public function getRole()
    {
        return $this->hasOne(UserRoles::className(), ['user_id' => 'id']);
    }

    public function getUser_roles()
    {
        return $this->hasOne(UserRoles::className(), ['user_id' => 'id']);
    }

    public static function findIdentity($id){
        return User::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        throw new NotSupportedException();//I don't implement this method because I don't have any access token column in my database
    }

    public function getId(){
        return $this->id;
    }

    public function getAuthKey(){
        throw new NotSupportedException();
    }

    public function validateAuthKey($authKey){
        throw new NotSupportedException();
    }
    public static function findByUsername($username){
        return User::findOne(['username'=>$username]);
    }

    public function validatePassword($password){
        return $this->password === $password;
    }
}