<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 25/01/2018
 * Time: 11:41
 */

namespace app\models;

use yii\db\ActiveRecord;

class Job extends ActiveRecord
{
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id' => 'location_id']);
    }
}