<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;
use app\models\UserRoles;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $first_name;
    public $last_name;
    public $username;
    public $password;
    public $phone;
    public $role;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['first_name', 'last_name', 'username', 'password', 'role'], 'required'],
            ['username', 'email', 'message' => 'Email is not a valid email address.'],
            ['username', 'validateUniqueUsername'],
            'first_name' => [['first_name'], 'string', 'max' => 40],
            'last_name' => [['last_name'], 'string', 'max' => 40],
            'phone' => [['phone'], 'match', 'pattern' => '/((\+[0-9]{6})|0)[-]?[0-9]{7}/'],
        ];
    }

    public function validateUniqueUsername($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if(User::checkUserExistsByUsername($this->username) != 0) {
                $this->addError($attribute, 'Email already exists');
            }
        }
    }

    /**
     * Registers a user with the information provided.
     * @return bool whether the registered is logged in successfully
     */
    public function register()
    {
        if ($this->validate()) {

            //TODO - Use htmlentities to help prevent XSS.

            //Create a new user object and store the form values into the database.
            $new_user = new User();
            $new_user->first_name = $this->first_name;
            $new_user->last_name = $this->last_name;
            $new_user->username = $this->username;
            $new_user->password = $hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);

            //Phone number is optional, do a check to see if their is a value set.
            if(!is_null($this->phone) && strlen($this->phone) > 0) {
                $new_user->phone = $this->phone;
            }

            $new_user->insert();

            //Now that the user has been created we need to create a record of their role in the composite table.
            //For now default is 1 but will change in the future.
            //Reason for a composite table is in case we wish to upscale in the future and provide more user roles or sub roles.
            $new_role = new UserRoles();
            $new_role->user_id = $new_user->id;
            $new_role->role_id = $this->role;
            $new_role->insert();

            //User has registered and we are going to log them in, set the role id for use later.
            \Yii::$app->session->set('user.role_id',$this->role);

            return Yii::$app->user->login($new_user, 0);
        }
        return false;
    }
}
