<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\UserJobs;

/**
 * ApplyForm is the model behind the Apply form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ApplyForm extends Model
{
    public $cv;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['cv'], 'file', 'skipOnEmpty' => false, 'extensions' => 'txt, doc, docx'],
        ];
    }

    /**
     * Allows the user to apply to a job granted the validation is met.
     *
     * Param: int $job_id - id of the Job listing the user is applying too.
     */
    public function apply($job_id)
    {
        if ($this->validate()) {
            $new_applicant = new UserJobs();
            $new_applicant->user_id = Yii::$app->user->identity->id;
            $new_applicant->job_id = $job_id;

            $file_name = $this->cv->baseName . '-' . Yii::$app->user->identity->id . '.' . $this->cv->extension;

            $new_applicant->cv = $file_name;
            $new_applicant->status = 'Pending';
            $new_applicant->save();

            $this->cv->saveAs('uploads/' . $file_name);
            return true;
        }
        return false;
    }
}
