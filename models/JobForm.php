<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Job;


/**
 * JobForm is the form model behind the add and edit job pages.
 */
class JobForm extends Model
{
    public $company_name;
    public $title;
    public $description;
    public $location_id;
    public $tags;
    public $salary;
    public $expiry_date;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['company_name', 'title', 'description', 'salary', 'expiry_date', 'location_id'], 'required'],
            'company_name' => [['company_name'], 'string', 'max' => 40],
            'title' => [['title'], 'string', 'max' => 40],
            'description' => [['description'], 'string', 'max' => 1500],
            'tags' => [['tags'], 'string', 'max' => 200],
            ['expiry_date', 'validateExpiryDate'],
        ];
    }

    /**
     * Used to validate that the expiry date is a date in the future and not in the past.
     */
    public function validateExpiryDate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $date = \DateTime::createFromFormat('d/m/Y', $this->expiry_date);
            if(strtotime($date->format('d-m-Y')) < strtotime('now')) {
                $this->addError($attribute, 'Expiry date must be after today\'s date');
            }
        }
    }

    /**
     * Add a new job listing
     */
    public function add()
    {
        if ($this->validate()) {
            //Create a new user object and store the form values into the database.
            $job = new Job();
            $job = $this->setData($job);

            return $job->insert();
        }
        return false;
    }

    /**
     * Edit a job listing.
     *
     * Param: Object $job - The Job the user wishes to edit.
     */
    public function edit($job) {
        if ($this->validate()) {
            $job = $this->setData($job);
            $job->save();

            return true;
        }

        return false;
    }

    /**
     * Take the Job model object and set the values.
     *
     * Returns the job model with the updated values.
     */
    public function setData($job) {
        $job->company_name = $this->company_name;
        $job->title = $this->title;
        $job->description = $this->description;
        $job->location_id = $this->location_id;
        $job->salary = $this->salary;

        //Format for db date is different. Create and change the format of the date to one that can be stored in the db.
        $date = \DateTime::createFromFormat('d/m/Y', $this->expiry_date);
        $job->expiry_date = $date->format('Y-m-d');

        //Phone number is optional, do a check to see if their is a value set.
        if(!is_null($this->tags) && strlen($this->tags) > 0) {
            $job->tags = $this->tags;
        }

        $job->user_id = Yii::$app->user->identity->id;

        return $job;
    }
}
