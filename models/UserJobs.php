<?php
/**
 * Created by PhpStorm.
 * User: developer
 * Date: 25/01/2018
 * Time: 11:41
 */

namespace app\models;

use yii\db\ActiveRecord;

class UserJobs extends ActiveRecord
{
    public function getJob()
    {
        return $this->hasOne(Job::className(), ['id' => 'job_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}